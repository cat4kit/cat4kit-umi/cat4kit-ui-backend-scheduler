# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

autodocsumm
sphinxcontrib-django
sphinx-design
django-environ
git+https://codebase.helmholtz.cloud/hcdc/hereon-netcdf/sphinxext.git
