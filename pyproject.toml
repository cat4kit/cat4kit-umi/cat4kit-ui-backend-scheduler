# SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0', 'versioneer[toml]']

[project]
name = "cat4kit-ui-backend-scheduler"
dynamic = ["version"]
description = "A scheduling application designed for the backend of Cat4KIT-UI."

readme = "README.md"
authors = [
    { name = 'Mostafa Hadizadeh', email = 'mostafa.hadizadeh@kit.edu' },
]
maintainers = [
    { name = 'Mostafa Hadizadeh', email = 'mostafa.hadizadeh@kit.edu' },
]
license = { text = 'EUPL-1.2' }

classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Typing :: Typed",
]
requires-python = '>= 3.8'
dependencies = [
    # add your dependencies here
    "Django>=3.2,<4.2",
    "types-requests",
]

[project.urls]
Homepage = 'https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-scheduler'
Documentation = "https://cat4kit-ui-backend-scheduler.readthedocs.io/en/latest/"
Source = "https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-scheduler"
Tracker = "https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-scheduler/issues/"


[project.optional-dependencies]
testsite = [
    "tox",
    "isort==5.12.0",
    "black==23.1.0",
    "blackdoc==0.3.8",
    "flake8==6.0.0",
    "pre-commit",
    "mypy",
    "pytest-cov",
    "reuse",
    "cffconvert",
    "django-environ",
    "django-stubs",
    "pytest-django",
    "drf_api_logger",
    "django",
    "djangorestframework",
    "djangorestframework-simplejwt",
    "django-cors-headers",
    "ds2stac_ingester",
    "TDS2STAC==2.2.0",
    "django_helmholtz_aai",
    "types-six",
    "six",
    "psycopg2",
]
docs = [
    "autodocsumm",
    "sphinx-rtd-theme",
    "hereon-netcdf-sphinxext",
    "sphinx-design",
    "django-environ",
    "myst_parser",
]
dev = [
    "cat4kit-ui-backend-scheduler[testsite]",
    "cat4kit-ui-backend-scheduler[docs]",
    "PyYAML",
    "types-PyYAML",
]


[tool.mypy]
ignore_missing_imports = true
plugins = [
    "mypy_django_plugin.main",
]

[tool.django-stubs]
django_settings_module = "cat4kit_ui_backend_scheduler.settings"

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
cat4kit_ui_backend_scheduler = ["py.typed"]

[tool.setuptools.packages.find]
namespaces = false
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'
DJANGO_SETTINGS_MODULE = "cat4kit_ui_backend_scheduler.settings"

[tool.versioneer]
VCS = 'git'
style = 'pep440'
versionfile_source = 'cat4kit_ui_backend_scheduler/_version.py'
versionfile_build = 'cat4kit_ui_backend_scheduler/_version.py'
tag_prefix = 'v'
parentdir_prefix = 'cat4kit-ui-backend-scheduler-'

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["cat4kit_ui_backend_scheduler"]
float_to_top = true
known_first_party = "cat4kit_ui_backend_scheduler"

[tool.black]
line-length = 79
target-version = ['py39']

[tool.coverage.run]
omit = ["cat4kit_ui_backend_scheduler/_version.py"]
