# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import os
import sys
import traceback
from datetime import datetime

import django
import requests
import yaml
from django.conf import settings
from django.http import JsonResponse
from insupdel4stac import InsUpDel4STAC

# isort: off
sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()

from tds2stacscheduler.models import (  # noqa: E402 # isort:skip
    APIHarvesterIngesterScheduler,
)

# isort: on


def ingestion_scheduler_objects(instance):
    manual_req = {
        "general_status": "pending",
        "ingesting_status": "pending",
        "ingesting_datetime": None,
    }
    try:
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Started"
        manual_req["general_status"] = "Ingesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        logger_name_get = (
            instance.job_name.strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )
        logger_id_get = instance.id
        stac_dir_get = (
            instance.stac_dir
            + "/"
            + logger_name_get
            + "_"
            + str(logger_id_get)
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": instance.id,
                "logger_name": instance.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response_ingesting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response_ingesting.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname") for log in response_ingesting.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=instance.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis_ingesting:
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=instance.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=instance.id, defaults=manual_req
                    )
            response_ingesting.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )

        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


cc = yaml.load(
    open("/app/scheduler-backup/schedulers.yml"), Loader=yaml.FullLoader
)
for i in cc["Schedulers"]:
    # create instance of model
    scheduler_data = cc["Schedulers"][i]

    try:
        instance = APIHarvesterIngesterScheduler(**scheduler_data)
        instance.save()
        # then run the ingester to sync the data
        ingestion_scheduler_objects(instance)
    except Exception as e:
        print(e)
        continue
