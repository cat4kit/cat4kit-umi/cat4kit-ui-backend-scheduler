# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import os
import sys


import django
import yaml


# isort: off
sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()

from tds2stacscheduler.models import (  # noqa: E402 # isort:skip
    APIHarvesterIngesterScheduler,
)

# isort: on


cc = yaml.load(
    open("/app/scheduler-backup/schedulers.yml"), Loader=yaml.FullLoader
)
for i in cc["Schedulers"]:
    # create instance of model
    scheduler_data = cc["Schedulers"][i]

    try:
        instance = APIHarvesterIngesterScheduler(**scheduler_data)
        instance.save()

    except Exception as e:
        print(e)
        continue
