# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import os
import sys
import traceback

import django
import yaml
from git import Repo
from django.conf import settings

# isort: off
sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()

from tds2stacscheduler.serializers import (  # noqa: E402 # isort:skip
    APIHarvesterIngesterSerializer,
)
from tds2stacscheduler.models import (  # noqa: E402 # isort:skip
    APIHarvesterIngesterScheduler,
)

# isort: on


def production_objects():
    schedulers = APIHarvesterIngesterScheduler.objects.all()
    serializer_scheduler = APIHarvesterIngesterSerializer(
        schedulers, many=True
    )
    scheduler_list = dict(Schedulers=dict())

    for index, scheduler in enumerate(serializer_scheduler.data):
        scheduler_detailed = APIHarvesterIngesterScheduler.objects.filter(
            id=scheduler["id"]
        ).values()
        scheduler_list["Schedulers"]["scheduler_" + str(index)] = dict(
            id=str(scheduler_detailed[0]["id"]),
            # engine="TDS2STAC",
            job_name=scheduler_detailed[0]["job_name"],
            email=scheduler_detailed[0]["email"],
            catalog_url=scheduler_detailed[0]["catalog_url"],
            stac_dir=scheduler_detailed[0]["stac_dir"],
            stac_id=scheduler_detailed[0]["stac_id"],
            stac_title=scheduler_detailed[0]["stac_title"],
            depth_number=scheduler_detailed[0]["depth_number"],
            depth_number_max=scheduler_detailed[0]["depth_number_max"],
            stac_desc=scheduler_detailed[0]["stac_desc"],
            spatial_information=scheduler_detailed[0]["spatial_information"],
            temporal_format_by_dataname=scheduler_detailed[0][
                "temporal_format_by_dataname"
            ],
            item_geometry_linestring=scheduler_detailed[0][
                "item_geometry_linestring"
            ],
            extra_metadata=scheduler_detailed[0]["extra_metadata"],
            collection_extra_field_list=scheduler_detailed[0][
                "collection_extra_field_list"
            ],
            item_properties_list=scheduler_detailed[0]["item_properties_list"],
            collection_custom_asset_list=scheduler_detailed[0][
                "collection_custom_asset_list"
            ],
            item_custom_asset_list=scheduler_detailed[0][
                "item_custom_asset_list"
            ],
            asset_properties=scheduler_detailed[0]["asset_properties"],
            extension_properties=scheduler_detailed[0]["extension_properties"],
            item_custom_extension_list=scheduler_detailed[0][
                "item_custom_extension_list"
            ],
            datacube_extension=scheduler_detailed[0]["datacube_extension"],
            webservice_properties=scheduler_detailed[0][
                "webservice_properties"
            ],
            requests_properties=scheduler_detailed[0]["requests_properties"],
            thumbnail_img=scheduler_detailed[0]["thumbnail_img"],
            overview_img=scheduler_detailed[0]["overview_img"],
            aggregated_url=scheduler_detailed[0]["aggregated_url"],
            auto_collection_ids=scheduler_detailed[0]["auto_collection_ids"],
            auto_collection_titles=scheduler_detailed[0][
                "auto_collection_titles"
            ],
            auto_collection_descriptions=scheduler_detailed[0][
                "auto_collection_descriptions"
            ],
            client_collection_ids=scheduler_detailed[0][
                "client_collection_ids"
            ],
            client_collection_titles=scheduler_detailed[0][
                "client_collection_titles"
            ],
            client_collection_descriptions=scheduler_detailed[0][
                "client_collection_descriptions"
            ],
            created_at=scheduler_detailed[0]["created_at"],
            harvesting_datetime=scheduler_detailed[0]["harvesting_datetime"],
            ingesting_datetime=scheduler_detailed[0]["ingesting_datetime"],
            harvesting_status=scheduler_detailed[0]["harvesting_status"],
            ingesting_status=scheduler_detailed[0]["ingesting_status"],
            general_status=scheduler_detailed[0]["general_status"],
            # queue_id=str(scheduler_detailed[0]["queue_id"]),
            # name=scheduler_detailed[0]["name"],
            # catalog=scheduler_detailed[0]["catalog"],
            # stac_id=scheduler_detailed[0]["stac_id"],
            # stac_desc=scheduler_detailed[0]["stac_desc"],
            # service=scheduler_detailed[0]["service"],
            # aggregated=scheduler_detailed[0]["aggregated"],
            # aggregated_url=scheduler_detailed[0]["aggregated_url"],
            # auto_collections=scheduler_detailed[0]["auto_collections"],
            # your_collections_ids=scheduler_detailed[0]["your_collections_ids"],
            # your_collections_descriptions=scheduler_detailed[0][
            #     "your_collections_descriptions"
            # ],
        )
    return scheduler_list


data = production_objects()


with open("/app/scheduler-backup/schedulers.yml", "w") as outfile:
    yaml.dump(data, outfile, default_flow_style=False)


os.chdir(r"/app/scheduler-backup")

PATH_OF_GIT_REPO = r".git"  # make sure .git folder is properly configured
COMMIT_MESSAGE = "Pushing from backend scheduler"
USERNAME = settings.GITLAB_USERNAME  # Replace with your GitHub username
PASSWORD = settings.GITLAB_PASSWORD  # Replace with your GitHub password


def git_push():
    try:
        print("Trying to push the code")
        repo = Repo(PATH_OF_GIT_REPO)
        repo.git.add(all=True)
        repo.index.commit(COMMIT_MESSAGE)
        remote_url = f"https://{USERNAME}:{PASSWORD}@codebase.helmholtz.cloud/cat4kit/scheduler-backup.git"
        origin = repo.create_remote("origin_with_credentials", url=remote_url)

        # Execute the push operation and capture the result
        push_info = origin.push()

        # Process the push_info to display push details
        for info in push_info:
            print(f"Push to {info.summary}")

    except Exception as e:
        # This captures and prints the traceback of the exception
        print(traceback.format_exc())
        print("An error occurred while pushing the code:" + str(e))


git_push()
