# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import os
import sys
import traceback
from datetime import datetime, timedelta

import django
from ds2stac_ingester import ds2stac_ingester
from tds2stac import app

sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()
from scheduler.models import SchedulerTDS2STAC  # noqa: E402 # isort:skip
from scheduler.serializers import (  # noqa: E402 # isort:skip
    SchedulerTDS2STACMainPageSerializer,
)

# Print the current working directory
print("Current working directory: {0}".format(os.getcwd()))

# Change the current working directory
os.chdir("/app/cat4kit-ui-backend-scheduler")

# Print the current working directory
print("Current working directory: {0}".format(os.getcwd()))


def querydict_to_dict(query_dict):
    data = {}
    for key in query_dict.keys():
        v = query_dict.getlist(key)
        if len(v) == 1:
            v = v[0]
        data[key] = v
    return data


def merge(list1, list2, list3):
    merged_list = [
        (
            p1.strip().replace("\n", "\n\n").replace('"', ""),
            p2.strip().replace("\n", "\n\n").replace('"', ""),
            p3.strip().replace("\n", "\n\n").replace('"', ""),
        )
        for idx1, p1 in enumerate(list1)
        for idx2, p2 in enumerate(list2)
        for idx3, p3 in enumerate(list3)
        if idx1 == idx2
    ]
    return merged_list


def get_scheduler_objects():
    schedulers = SchedulerTDS2STAC.objects.all()
    serializer = SchedulerTDS2STACMainPageSerializer(schedulers, many=True)
    return serializer.data


list_of_schedulers = get_scheduler_objects()


def run_scheduler(scheduler_list):
    for scheduler in list_of_schedulers:
        try:
            scheduler_detailed = SchedulerTDS2STAC.objects.filter(
                id=scheduler["id"]
            ).values()

            if scheduler_detailed[0]["ingesting_status"] == "finished":
                now = datetime.now()
                yesterday = now - timedelta(days=1)
                now_formatted = now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                yesterday_formatted = yesterday.strftime(
                    "%Y-%m-%dT%H:%M:%S.%fZ"
                )

                datetime_filter_value = [yesterday_formatted, now_formatted]
            else:
                datetime_filter_value = None

            req = {
                "general_status": None,
                "harvesting_datetime": None,
                "harvesting_status": None,
                "ingesting_datetime": None,
                "ingesting_status": None,
            }
            SchedulerTDS2STAC.objects.update_or_create(
                id=scheduler_detailed[0]["id"], defaults=req
            )
            if len(scheduler_detailed[0]["auto_collections"]) != 0:
                auto_collections = [
                    "" if x is None else x
                    for x in scheduler_detailed[0]["auto_collections"]
                ]
                your_collections_ids = [
                    "" if x is None else x
                    for x in scheduler_detailed[0]["your_collections_ids"]
                ]
                your_collections_descriptions = [
                    "" if x is None else x
                    for x in scheduler_detailed[0][
                        "your_collections_descriptions"
                    ]
                ]
                merged_collection_tuples = merge(
                    auto_collections,
                    your_collections_ids,
                    your_collections_descriptions,
                )
            else:
                merged_collection_tuples = None
            stac_dir_get = (
                "/app/stac/scheduler/Scheduler_"
                + scheduler_detailed[0]["name"]
                + "_"
                + str(scheduler_detailed[0]["id"])
            )
            req["harvesting_datetime"] = datetime.now()
            req["harvesting_status"] = "started"
            req["general_status"] = "harvesting"
            SchedulerTDS2STAC.objects.update_or_create(
                id=scheduler_detailed[0]["id"], defaults=req
            )
            app.Harvester(
                scheduler_detailed[0]["catalog"],
                logger_handler="HTTPHandler",
                logger_id=scheduler_detailed[0]["id"],
                logger_name="Scheduler_" + scheduler_detailed[0]["name"],
                logger_handler_host="cat4kit.atmohub.kit.edu",
                logger_handler_url="/oncetask/api/logger/logger_post_scheduler/",
                collection_tuples=merged_collection_tuples,
                stac=True,
                stac_dir=stac_dir_get,
                stac_id=scheduler_detailed[0]["stac_id"],
                stac_description=scheduler_detailed[0]["stac_desc"],
                web_service=scheduler_detailed[0]["service"].lower(),
                datetime_filter=datetime_filter_value,
                aggregated_dataset=scheduler_detailed[0]["aggregated"],
                aggregated_dataset_url=scheduler_detailed[0]["aggregated_url"],
            )
            if os.path.exists(stac_dir_get + "/stac/"):
                req["harvesting_datetime"] = datetime.now()
                req["harvesting_status"] = "finished"
                SchedulerTDS2STAC.objects.update_or_create(
                    id=scheduler_detailed[0]["id"], defaults=req
                )
            else:
                req["harvesting_datetime"] = datetime.now()
                req["harvesting_status"] = "finished"
                req["general_status"] = "Harvesting failed"
                SchedulerTDS2STAC.objects.update_or_create(
                    id=scheduler_detailed[0]["id"], defaults=req
                )
                continue
        except Exception:
            req["harvesting_datetime"] = datetime.now()
            req["harvesting_status"] = "finished"
            req["general_status"] = "Harvesting failed"
            SchedulerTDS2STAC.objects.update_or_create(
                id=scheduler_detailed[0]["id"], defaults=req
            )
            print(traceback.format_exc())
            print(
                "Error: Harvesting failed. Please check the server status and try again."
            )

            continue
        else:
            try:
                req["general_status"] = "Ingesting"
                req["ingesting_datetime"] = datetime.now()
                req["ingesting_status"] = "started"
                SchedulerTDS2STAC.objects.update_or_create(
                    id=scheduler_detailed[0]["id"], defaults=req
                )
                ds2stac_ingester.Ingester(
                    stac_dir=stac_dir_get,
                    logger_handler="HTTPHandler",
                    logger_id=scheduler_detailed[0]["id"],
                    logger_name="Scheduler_" + scheduler_detailed[0]["name"],
                    logger_handler_host="cat4kit.atmohub.kit.edu",
                    logger_handler_url="/oncetask/api/logger/logger_post_scheduler/",
                    API_posting=False,
                )
                req["ingesting_datetime"] = datetime.now()
                req["ingesting_status"] = "finished"
                req["general_status"] = "Successfully done!"
                SchedulerTDS2STAC.objects.update_or_create(
                    id=scheduler_detailed[0]["id"], defaults=req
                )
            except Exception:
                req["ingesting_datetime"] = datetime.now()
                req["ingesting_status"] = "finished"
                req["general_status"] = "Ingesting failed"
                SchedulerTDS2STAC.objects.update_or_create(
                    id=scheduler_detailed[0]["id"], defaults=req
                )
                print(traceback.format_exc())
                print(
                    "Error: Ingestion failed. Please check the server status and try again."
                )
                continue


run_scheduler(list_of_schedulers)
