import os
import sys
import traceback

# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import django
import yaml
from git import Repo

sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()

from scheduler.models import SchedulerTDS2STAC  # noqa: E402 # isort:skip
from scheduler.serializers import (  # noqa: E402 # isort:skip
    SchedulerTDS2STACMainPageSerializer,
)


def scheduler_objects():
    schedulers = SchedulerTDS2STAC.objects.all()
    serializer_scheduler = SchedulerTDS2STACMainPageSerializer(
        schedulers, many=True
    )
    scheduler_list = dict(Schedulers=dict())

    for scheduler in serializer_scheduler.data:
        scheduler_detailed = SchedulerTDS2STAC.objects.filter(
            id=scheduler["id"]
        ).values()
        scheduler_list["Schedulers"][
            "scheduler_" + str(scheduler["queue_id"])
        ] = dict(
            queue_id=str(scheduler_detailed[0]["queue_id"]),
            name=scheduler_detailed[0]["name"],
            catalog=scheduler_detailed[0]["catalog"],
            stac_id=scheduler_detailed[0]["stac_id"],
            stac_desc=scheduler_detailed[0]["stac_desc"],
            service=scheduler_detailed[0]["service"],
            aggregated=scheduler_detailed[0]["aggregated"],
            aggregated_url=scheduler_detailed[0]["aggregated_url"],
            auto_collections=scheduler_detailed[0]["auto_collections"],
            your_collections_ids=scheduler_detailed[0]["your_collections_ids"],
            your_collections_descriptions=scheduler_detailed[0][
                "your_collections_descriptions"
            ],
        )
    return scheduler_list


data = scheduler_objects()


with open("/app/scheduler-backup/schedulers.yml", "w") as outfile:
    yaml.dump(data, outfile, default_flow_style=False)


os.chdir(r"/app/scheduler-backup")

PATH_OF_GIT_REPO = r".git"  # make sure .git folder is properly configured
COMMIT_MESSAGE = "Pushing from backend scheduler"


def git_push():
    try:
        repo = Repo(PATH_OF_GIT_REPO)
        repo.git.add(all=True)
        repo.index.commit(COMMIT_MESSAGE)
        origin = repo.remote(name="origin")
        origin.push()
    except Exception:
        print(traceback.format_exc())
        print("Some error occured while pushing the code")


git_push()
