import os
import sys

# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
import django
import yaml

sys.path.append(
    os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
)  # set the directory we are working in (django manage.py directory)
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "cat4kit_ui_backend_scheduler.settings"
)
django.setup()
from scheduler.models import SchedulerTDS2STAC  # noqa: E402 # isort:skip


cc = yaml.load(
    open("/app/scheduler-backup/schedulers.yml"), Loader=yaml.FullLoader
)
for i in cc["Schedulers"]:
    # create instance of model
    try:
        M = SchedulerTDS2STAC(**cc["Schedulers"][i])
        M.save()
    except Exception as e:
        print(e)
        continue
