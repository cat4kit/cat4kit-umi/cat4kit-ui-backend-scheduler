# SPDX-FileCopyrightText: 2023 2023 Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0


import json
import os
import shutil
import traceback

# from .serializers import APIHarvesterSerializer
from datetime import datetime

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import JsonResponse
from insupdel4stac import InsUpDel4STAC

# from logger.models import Logger
# from logger.serializers import LoggerNameSerializer, LoggerSerializer
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.pagination import PageNumberPagination
from tds2stac import (
    NestedCollectionInspector,
    Recognizer,
    TDS2STACIntegrator,
    WebServiceListScraper,
)

from .forms import (
    InitialRecognizingForm,
    NestedCollectionForm,
    SaveCloseForm,
    SaveHarvestIngestCloseForm,
    SaveUpdateCloseForm,
)
from .models import APIHarvesterIngesterScheduler
from .serializers import (
    APIHarvesterIngesterDepthClientSerializer,
    APIHarvesterIngesterSelectedSerializer,
    APIHarvesterIngesterSerializer,
)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_recognizing_initial_trigger(request):
    """
    This is a function for running the Recognizer and Nested_Collection functions
    of tds2stac.
    """
    print(request.data)
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB-Scheduler"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    if (
        request.data["spatial_information"] == "[]"
        or request.data["spatial_information"].isspace()
    ):
        request.data["spatial_information"] = None
    if (
        request.data["temporal_format_by_dataname"] == "null"
        or request.data["temporal_format_by_dataname"].isspace()
    ):
        request.data["temporal_format_by_dataname"] = None
    if (
        request.data["aggregated_url"] == "null"
        or request.data["aggregated_url"].isspace()
    ):
        request.data["aggregated_url"] = None
    if (
        request.data["depth_number_max"] == "null"
        or request.data["depth_number_max"].isspace()
    ):
        request.data["depth_number_max"] = None
    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Making labels for the fields that are not filled by the user
    manual_req = {
        "created_at": datetime.now(),
        "depth_number_max": 10,
        "general_status": "pending",
        "harvesting_datetime": None,
        "harvesting_status": "pending",
        "ingesting_datetime": None,
        "ingesting_status": "pending",
        "auto_collection_ids": [],
        "auto_collection_titles": [],
        "auto_collection_descriptions": [],
        "client_collection_ids": [],
        "client_collection_titles": [],
        "client_collection_descriptions": [],
    }

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    print(request.data["asset_properties"])
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    form = InitialRecognizingForm(request.data, request.FILES)

    if form.is_valid():
        saved_form = form.save()
    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    # Making an appropriate logger_name and logger_id for the logger
    logger_name_get = (
        request.data["job_name"]
        .strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = saved_form.id

    # Sanitize the catalog_url
    catalog_url_get = request.data["catalog_url"].strip().replace("\n", "")

    try:
        manual_req["general_status"] = "Recognizing"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

        recognizer_output = Recognizer(
            main_catalog_url=catalog_url_get,
            nested_check=True,
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        recognizer_status = recognizer_output.status
        depth_number = recognizer_output.nested_num
        manual_req["depth_number_max"] = depth_number
        manual_req["general_status"] = "Recognized"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

    except Exception:
        manual_req["general_status"] = "Failed Recognizing"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in recognizing",
            },
            safe=False,
        )

    try:
        manual_req["general_status"] = "Processing Collections Inspector"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )
        nested_output = NestedCollectionInspector(
            main_catalog_url=catalog_url_get,
            nested_number=int(request.data["depth_number"]),
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        list_of_collections = list(nested_output)

        for i in range(len((list_of_collections))):
            manual_req["auto_collection_ids"].append(list_of_collections[i][1])
            manual_req["auto_collection_titles"].append(
                list_of_collections[i][2]
            )
            manual_req["auto_collection_descriptions"].append("")
            # Checking if there is no non-empy element, like "" or empty, in the manual_req["client_collection_ids"]
            print(
                "manual_req['client_collection_ids']",
                request.data.get("client_collection_ids"),
            )
            print(
                "manual_req['client_collection_titles']",
                request.data.get("client_collection_titles"),
            )
            print(
                "manual_req['client_collection_descriptions']",
                request.data.get("client_collection_descriptions"),
            )
            client_collection_ids = request.data.get("client_collection_ids")

            if client_collection_ids is not None:
                if all(
                    not element.strip()
                    for element in client_collection_ids.split(",")
                ):
                    manual_req["client_collection_ids"].append("")
                    manual_req["client_collection_titles"].append("")
                    manual_req["client_collection_descriptions"].append("")
            else:
                # Handle the case where client_collection_ids is None
                # Depending on your requirements, you can append an empty string or perform other actions.
                manual_req["client_collection_ids"].append("")
                manual_req["client_collection_titles"].append("")
                manual_req["client_collection_descriptions"].append("")
        if request.data.get("client_collection_ids") is not None:
            manual_req["client_collection_ids"] = request.data.get(
                "client_collection_ids"
            ).split(",")
        if request.data.get("client_collection_titles") is not None:
            manual_req["client_collection_titles"] = request.data.get(
                "client_collection_titles"
            ).split(",")
        if request.data.get("client_collection_descriptions") is not None:
            manual_req["client_collection_descriptions"] = request.data.get(
                "client_collection_descriptions"
            ).split(",")

        if recognizer_status not in [
            "First Scenario",
            "Second Scenario",
            "Third Scenario",
            "Eighth Scenario",
            "Ninth Scenario",
        ]:
            # TODO: Analysing logs for ERRORS, and if there is any error so far, we can save the object with an error label to make the color of label red in the frontend
            manual_req["general_status"] = "Non-Nested Collection"
            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=saved_form.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": "success",
                    "recognizer_status": str(recognizer_status),
                    "depth_number": (depth_number),
                    "nested_output": list(nested_output),
                },
                safe=False,
            )

        else:
            manual_req["general_status"] = "Nested Collection"
            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=saved_form.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": "Attention: This is a nested collection, and the auto-details of collections are obtained at depth zero. To modify the depth number, simply access the Nested Collection Inspector and retrieve the details of the nested collections once more.",
                    "recognizer_status": str(recognizer_status),
                    "depth_number": (depth_number),
                    "nested_output": list(nested_output),
                },
                safe=False,
            )

    except Exception:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Collections Inspector"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=saved_form.id, defaults=manual_req
        )

        return JsonResponse(
            {
                "message": "failed to find auto collections details",
            },
            safe=False,
        )


##############################################################################################################
# place of functions for Scheduler Jobs
##############################################################################################################


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngesterScheduler.objects.get(
                id=instance_id
            )
        except APIHarvesterIngesterScheduler.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None
    form = SaveCloseForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

        return JsonResponse(
            {
                "message": "success",
            },
            safe=False,
        )
    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_nestedcollection_close_by_id(request):
    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngesterScheduler.objects.get(
                id=instance_id
            )
        except APIHarvesterIngesterScheduler.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = NestedCollectionForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_schedulerjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_schedulerjob_object.id
    # Sanitize the catalog_url
    catalog_url_get = get_schedulerjob_object.catalog_url.strip().replace(
        "\n", ""
    )

    manual_req = {
        "general_status": "Processing Collections Inspector",
        "auto_collection_ids": [],
        "auto_collection_titles": [],
        "auto_collection_descriptions": [],
        "client_collection_ids": [],
        "client_collection_titles": [],
        "client_collection_descriptions": [],
    }
    APIHarvesterIngesterScheduler.objects.update_or_create(
        id=get_schedulerjob_object.id, defaults=manual_req
    )

    try:
        nested_output = NestedCollectionInspector(
            main_catalog_url=catalog_url_get,
            nested_number=int(get_schedulerjob_object.depth_number),
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        list_of_collections = list(nested_output)

        for i in range(len((list(list_of_collections)))):
            manual_req["auto_collection_ids"].append(list_of_collections[i][1])
            manual_req["auto_collection_titles"].append(
                list_of_collections[i][2]
            )
            manual_req["auto_collection_descriptions"].append("")
            manual_req["client_collection_ids"].append("")
            manual_req["client_collection_titles"].append("")
            manual_req["client_collection_descriptions"].append("")

            # TODO: Analysing logs for ERRORS, and if there is any error so far, we can save the object with an error label to make the color of label red in the frontend
        if get_schedulerjob_object.depth_number_max > 0:
            manual_req["general_status"] = "Nested Collection"
        else:
            manual_req["general_status"] = "Non-Nested Collection"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "success",
                "nested_output": list(nested_output),
            },
            safe=False,
        )

    except Exception:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Collections Inspector"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed to find auto collections details",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_harvest_ingest_close_by_id(request):
    request.data._mutable = True
    print(request.data)
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )
    if (
        request.data["spatial_information"] == "[]"
        or request.data["spatial_information"].isspace()
    ):
        request.data["spatial_information"] = None
    if (
        request.data["temporal_format_by_dataname"] == "null"
        or request.data["temporal_format_by_dataname"].isspace()
    ):
        request.data["temporal_format_by_dataname"] = None
    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    # request.data["webservice_properties"] = request.data[
    #     "webservice_properties"
    # ]
    if (
        request.data["aggregated_url"] == "null"
        or request.data["aggregated_url"].isspace()
    ):
        request.data["aggregated_url"] = None
    if (
        request.data["client_collection_ids"] == "null"
        or request.data["client_collection_ids"].isspace()
    ):
        request.data["client_collection_ids"] = None
    if (
        request.data["client_collection_titles"] == "null"
        or request.data["client_collection_titles"].isspace()
    ):
        request.data["client_collection_titles"] = None
    if (
        request.data["client_collection_descriptions"] == "null"
        or request.data["client_collection_descriptions"].isspace()
    ):
        request.data["client_collection_descriptions"] = None

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngesterScheduler.objects.get(
                id=instance_id
            )
        except APIHarvesterIngesterScheduler.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        request.data["job_name"]
        .strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = instance.id

    # Making a stac_dir
    stac_dir_get = (
        instance.stac_dir + "/" + logger_name_get + "_" + str(logger_id_get)
    )

    # Making a job_dir
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )

    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    manual_req = {
        "general_status": "pending",
        "harvesting_status": "pending",
        "harvesting_datetime": None,
        "ingesting_status": "pending",
        "ingesting_datetime": None,
    }

    APIHarvesterIngesterScheduler.objects.update_or_create(
        id=instance.id, defaults=manual_req
    )

    try:
        manual_req = {
            "general_status": "Deleting",
            "harvesting_status": request.data.get("harvesting_status")
            if request.data.get("harvesting_status") != "null"
            else None,
            "harvesting_datetime": request.data.get("harvesting_datetime")
            if request.data.get("harvesting_datetime") != "null"
            else None,
            "ingesting_status": request.data.get("ingesting_status")
            if request.data.get("ingesting_status") != "null"
            else None,
            "ingesting_datetime": request.data.get("ingesting_datetime")
            if request.data.get("ingesting_datetime") != "null"
            else None,
        }
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response_deleting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            response_deleting.raise_for_status()
        except requests.RequestException as logger_api_error:
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception as stac_error:
        print(traceback.format_exc())
        manual_req["general_status"] = "Failed Deleting"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=instance.id, defaults=manual_req
        )
        return JsonResponse(
            {"message": "Failed in deleting TestJobObject." + str(stac_error)},
            status=500,
            safe=False,
        )

    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)

            manual_req["general_status"] = "Deleted"
            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
        except Exception:
            print(traceback.format_exc())
            manual_req["general_status"] = "Deleted With Errors"
            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=instance.id, defaults=manual_req
            )
    else:
        print("Failed in deleting from local storage.")

    form = SaveHarvestIngestCloseForm(
        request.data, request.FILES, instance=instance
    )

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    # Sanitize the catalog_url
    catalog_url_get = get_schedulerjob_object.catalog_url.strip().replace(
        "\n", ""
    )

    # Sanitize the aggregated_url, TODO: we have to add a validator for this field
    if get_schedulerjob_object.aggregated_url is not None:
        aggregated_url_get = (
            get_schedulerjob_object.aggregated_url.strip().replace("\n", "")
        )
    else:
        aggregated_url_get = None

    # Sanitize the spatial_information
    spatial_information_list = get_schedulerjob_object.spatial_information
    if spatial_information_list != "" and len(spatial_information_list) == 4:
        if (
            spatial_information_list[0] == spatial_information_list[1]
            and spatial_information_list[2] == spatial_information_list[3]
        ):
            spatial_information_list = [
                spatial_information_list[0],
                spatial_information_list[2],
            ]
    else:
        spatial_information_list = None

    # Sanitize the temporal_information
    temporal_information_get = (
        get_schedulerjob_object.temporal_format_by_dataname
    )
    if get_schedulerjob_object.temporal_format_by_dataname is not None:
        if (
            get_schedulerjob_object.temporal_format_by_dataname.strip().replace(
                "\n", ""
            )
            == ""
        ):
            temporal_information_get = None
        else:
            temporal_information_get = get_schedulerjob_object.temporal_format_by_dataname.strip().replace(
                "\n", ""
            )

    # Sanitize the requests_properties
    requests_properties_dict = get_schedulerjob_object.requests_properties

    # stac_validation_input = get_schedulerjob_object.stac_validation_input
    # if stac_validation_input != {}:
    #     if stac_validation_input.get("staccatalog") is not False:
    #         print("activate a funtion to get the Catalog validation and fill the catalog_validation field in the model")
    #     if stac_validation_input.get("staccollection") is not False:
    #         print("activate a funtion to get the Collection validation and fill the collection_validation field in the model")
    #     if stac_validation_input.get("stacitem") is not False:
    #         print("activate a funtion to get the Item validation and fill the item_validation field in the model")

    os.makedirs(job_dir, exist_ok=True)
    os.makedirs(job_dir1, exist_ok=True)
    # Sanitize the asset_properties
    thumbnail_overview_url = settings.THUMBNAIL_OVERVIEW_URL
    print(
        "li", thumbnail_overview_url, get_schedulerjob_object.asset_properties
    )
    if (
        get_schedulerjob_object.asset_properties != {}
        and get_schedulerjob_object.asset_properties is not None
    ):
        if (
            get_schedulerjob_object.asset_properties.get(
                "collection_thumbnail_link"
            )
            is not None
        ):
            _, file_extension = os.path.splitext(
                get_schedulerjob_object.asset_properties[
                    "collection_thumbnail_link"
                ]
            )
            get_schedulerjob_object.asset_properties[
                "collection_thumbnail_link"
            ] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "thumbnail"
                + file_extension
            )
        # elif get_schedulerjob_object.asset_properties.get("collection_thumbnail_link") is None and get_schedulerjob_object.asset_properties.get("collection_thumbnail") == "link" and request.data["thumbnail_img"]:

        if (
            get_schedulerjob_object.asset_properties.get(
                "collection_overview_link"
            )
            is not None
        ):
            _, file_extension = os.path.splitext(
                request.data["asset_properties"]["collection_overview_link"]
            )
            get_schedulerjob_object.asset_properties[
                "collection_overview_link"
            ] = (
                str(thumbnail_overview_url)
                + logger_name_get
                + "-"
                + str(logger_id_get)
                + "-"
                + request.data["email"].replace("@", "-").replace(".", "-")
                + "/"
                + "overview"
                + file_extension
            )

    print(
        "get_schedulerjob_object.asset_properties",
        get_schedulerjob_object.asset_properties,
        request.data["thumbnail_img"],
    )

    # Sanitize the extra_metadata
    extra_metadata_file_path = os.path.join(job_dir, "extra_metadata.json")
    if get_schedulerjob_object.extra_metadata != {
        "collection": {"extra_fields": {}},
        "item": {"properties": {}},
    }:
        with open(extra_metadata_file_path, "w") as file:
            json.dump(
                dict(get_schedulerjob_object.extra_metadata), file, indent=4
            )
        extra_metadata_dict = {
            "extra_metadata": True,
            "extra_metadata_file": extra_metadata_file_path,
        }
    else:
        extra_metadata_dict = None

    # Sanitize the webservice_properties
    webservice_properties_dict = (
        get_schedulerjob_object.webservice_properties
    ).replace("\r\n", "\r\n")
    if webservice_properties_dict != {}:
        with open(job_dir + "/tag_file.json", "w") as file:
            file.write(webservice_properties_dict)
        # json.dump(webservice_properties_dict, file, indent=16)
    else:
        webservice_properties_dict = None

    # Sanitize the extension_properties

    extension_properties_dict = get_schedulerjob_object.extension_properties
    extension_properties_list = []
    for key, value in extension_properties_dict.items():
        if isinstance(extension_properties_dict[key], list):
            with open(job_dir1 + "/" + key, "w") as file:
                file.write(extension_properties_dict[key][2])
                extension_properties_list.append(
                    (
                        extension_properties_dict[key][0],
                        extension_properties_dict[key][1],
                        job_dir1 + "/" + key,
                    )
                )
        if isinstance(extension_properties_dict[key], str):
            extension_properties_list.append(extension_properties_dict[key])
    extension_properties_list.append("common_metadata")
    final_extension_propertie = {
        "item_extensions": extension_properties_list,
    }
    print("final_extension_propertie", final_extension_propertie)

    # Sanitize the client_collection details
    if len(get_schedulerjob_object.auto_collection_ids) != 0:
        if len(get_schedulerjob_object.client_collection_ids) != 0:
            # replace the None with "" in the client_collection_ids list
            client_collection_ids_modified = [
                "" if v is None else v
                for v in get_schedulerjob_object.client_collection_ids
            ]
        else:
            client_collection_ids_modified = [""]
        if len(get_schedulerjob_object.client_collection_titles) != 0:
            # replace the None with "" in the client_collection_titles list
            client_collection_titles_modified = [
                "" if v is None else v
                for v in get_schedulerjob_object.client_collection_titles
            ]
        else:
            client_collection_titles_modified = [""]
        if len(get_schedulerjob_object.client_collection_descriptions) != 0:
            # replace the None with "" in the client_collection_descriptions list
            client_collection_descriptions_modified = [
                "" if v is None else v
                for v in get_schedulerjob_object.client_collection_descriptions
            ]
        else:
            client_collection_descriptions_modified = [""]

        # merge the client_collection details with the auto_collection details

        merged_collection_tuples = list(
            zip(
                get_schedulerjob_object.auto_collection_ids,
                client_collection_ids_modified,
                client_collection_titles_modified,
                client_collection_descriptions_modified,
            )
        )
    else:
        merged_collection_tuples = None

    try:
        manual_req["general_status"] = "Harvesting"
        manual_req["harvesting_status"] = "Started"
        manual_req["harvesting_datetime"] = datetime.now()

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        """
        print("TDS2STAC runner",f'''TDS2STACIntegrator(
            TDS_catalog={catalog_url_get},
            stac_dir={stac_dir_get},
            stac_id={get_schedulerjob_object.stac_id},
            stac_title={get_schedulerjob_object.stac_title},
            stac_description={get_schedulerjob_object.stac_desc},
            collection_tuples={merged_collection_tuples},
            # datetime_filter=datetime_string_get,
            aggregated_dataset_url={aggregated_url_get},
            depth_number={int(get_schedulerjob_object.depth_number)},
            spatial_information={spatial_information_list},
            temporal_format_by_dataname={temporal_information_get},
            item_geometry_linestring={bool(
                get_schedulerjob_object.item_geometry_linestring
            )},
            #limited_number=5,
            extra_metadata={extra_metadata_dict},
            asset_properties={get_schedulerjob_object.asset_properties},
            extension_properties={final_extension_propertie},
            webservice_properties={
                "web_service_config_file": {job_dir} + "/tag_file.json"
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": {logger_id_get},
                "logger_name": {logger_name_get},
                "logger_handler_host": {settings.EXTERNAL_URL_CAT4KIT},
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
            requests_properties={requests_properties_dict},
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )''')
        print(f'''TDS2STACIntegrator(
            TDS_catalog={catalog_url_get},
            stac_dir={stac_dir_get},
            stac_id={get_schedulerjob_object.stac_id},
            stac_title={get_schedulerjob_object.stac_title},
            stac_description={get_schedulerjob_object.stac_desc},
            collection_tuples={merged_collection_tuples},
            # datetime_filter=datetime_string_get,
            aggregated_dataset_url={aggregated_url_get},
            depth_number={int(get_schedulerjob_object.depth_number)},
            spatial_information={spatial_information_list},
            temporal_format_by_dataname={temporal_information_get},
            item_geometry_linestring={bool(get_schedulerjob_object.item_geometry_linestring)},
            #limited_number=5,
            extra_metadata={extra_metadata_dict},
            asset_properties={get_schedulerjob_object.asset_properties},
            extension_properties={final_extension_propertie},
            webservice_properties={{
                "web_service_config_file": {job_dir} + "/tag_file.json"
            }},
            logger_properties={{
                "logger_handler": "HTTPHandler",
                "logger_id": {logger_id_get},
                "logger_name": {logger_name_get},
                "logger_handler_host": {settings.EXTERNAL_URL_CAT4KIT},
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            }},
            requests_properties={requests_properties_dict,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )''')

        """
        print(
            f"TDS2STACIntegrator(TDS_catalog={catalog_url_get}, stac_dir={stac_dir_get}, stac_id={get_schedulerjob_object.stac_id}, stac_title={get_schedulerjob_object.stac_title}, stac_description={get_schedulerjob_object.stac_desc}, collection_tuples={merged_collection_tuples}, aggregated_dataset_url={aggregated_url_get}, depth_number={int(get_schedulerjob_object.depth_number)}, spatial_information={spatial_information_list}, temporal_format_by_dataname={temporal_information_get}, item_geometry_linestring={bool(get_schedulerjob_object.item_geometry_linestring)}, extra_metadata={extra_metadata_dict}, asset_properties={get_schedulerjob_object.asset_properties}, extension_properties={final_extension_propertie}, webservice_properties={{'web_service_config_file': {job_dir} + '/tag_file.json'}}, logger_properties={{'logger_handler': 'HTTPHandler', 'logger_id': {logger_id_get}, 'logger_name': {logger_name_get}, 'logger_handler_host': {settings.EXTERNAL_URL_CAT4KIT}, 'logger_handler_port': '8003', 'logger_handler_url': '/api/logger/logger_post/', 'logger_handler_method': 'POST'}}, requests_properties={requests_properties_dict})"
        )
        TDS2STACIntegrator(
            TDS_catalog=catalog_url_get,
            stac_dir=stac_dir_get,
            stac_id=get_schedulerjob_object.stac_id,
            stac_title=get_schedulerjob_object.stac_title,
            stac_description=get_schedulerjob_object.stac_desc,
            collection_tuples=merged_collection_tuples,
            # datetime_filter=datetime_string_get,
            aggregated_dataset_url=aggregated_url_get,
            depth_number=int(get_schedulerjob_object.depth_number),
            spatial_information=spatial_information_list,
            temporal_format_by_dataname=temporal_information_get,
            item_geometry_linestring=bool(
                get_schedulerjob_object.item_geometry_linestring
            ),
            # limited_number=50,
            extra_metadata=extra_metadata_dict,
            asset_properties=get_schedulerjob_object.asset_properties,
            extension_properties=final_extension_propertie,
            webservice_properties={
                "web_service_config_file": job_dir + "/tag_file.json"
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": logger_id_get,
                "logger_name": logger_name_get,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
            requests_properties=requests_properties_dict,
            # logger_properties = {"logger_handler": "StreamHandler", "logger_id": logger_id_get, "logger_name": logger_name_get}
        )

        # Analysing the logs
        try:
            response_harvesting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response_harvesting.status_code == 200:
                log_analysis = [
                    log.get("levelname")
                    for log in response_harvesting.json()
                    if log not in response_deleting.json()
                ]
                if "CRITICAL" in log_analysis:
                    manual_req["general_status"] = "Failed Harvesting"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Failed"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in harvesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis:
                    manual_req["general_status"] = "Harvested with Errors"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )

                else:
                    manual_req["general_status"] = "Harvested"
                    manual_req["harvesting_datetime"] = datetime.now()
                    manual_req["harvesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )

            response_harvesting.raise_for_status()
        except requests.RequestException as logger_api_error:
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["harvesting_datetime"] = datetime.now()
        manual_req["harvesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Harvesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in harvesting",
            },
            safe=False,
        )

    try:
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Started"
        manual_req["general_status"] = "Ingesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        print("stac", stac_dir_get + "/stac/")

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_schedulerjob_object.id,
                "logger_name": get_schedulerjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response_ingesting = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response_ingesting.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname")
                    for log in response_ingesting.json()
                    if log not in response_harvesting.json()
                    and log not in response_deleting.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" not in log_analysis
                ):
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                elif (
                    "ERROR" in log_analysis_ingesting
                    and "ERROR" in log_analysis
                ):
                    manual_req[
                        "general_status"
                    ] = "Harvested and Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
            response_ingesting.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )

        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=get_schedulerjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_ingest_close_by_id(request):
    """
    This is a function for ingestion by id.
    """

    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]

    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngesterScheduler.objects.get(
                id=instance_id
            )
        except APIHarvesterIngesterScheduler.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = SaveHarvestIngestCloseForm(
        request.data, request.FILES, instance=instance
    )

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_schedulerjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_schedulerjob_object.id

    manual_req = {
        "ingesting_datetime": datetime.now(),
        "ingesting_status": "Started",
        "general_status": "Ingesting",
    }

    # Making a stac_dir
    stac_dir_get = (
        get_schedulerjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    try:
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Insert",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_schedulerjob_object.id,
                "logger_name": get_schedulerjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

        # Analysing the logs
        try:
            response = requests.get(
                f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_get/"
                + str(logger_name_get)
                + "_"
                + str(logger_id_get)
                + "/"
            )

            if response.status_code == 200:
                log_analysis_ingesting = [
                    log.get("levelname") for log in response.json()
                ]
                if "CRITICAL" in log_analysis_ingesting:
                    manual_req["general_status"] = "Failed Ingesting"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                    return JsonResponse(
                        {
                            "message": "failed in ingesting",
                        },
                        safe=False,
                    )
                elif "ERROR" in log_analysis_ingesting:
                    manual_req["general_status"] = "Ingested with Errors"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )
                else:
                    manual_req["general_status"] = "Ingested"
                    manual_req["ingesting_datetime"] = datetime.now()
                    manual_req["ingesting_status"] = "Finished"
                    APIHarvesterIngesterScheduler.objects.update_or_create(
                        id=get_schedulerjob_object.id, defaults=manual_req
                    )

            response.raise_for_status()
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except requests.RequestException as logger_api_error:
            manual_req["ingesting_datetime"] = datetime.now()
            manual_req["ingesting_status"] = "Failed"
            manual_req["general_status"] = "Failed Analysing Logger"

            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=get_schedulerjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in analysing Logger via API: {str(logger_api_error)}"
                },
                status=500,
            )
    except Exception:
        print(traceback.format_exc())
        manual_req["ingesting_datetime"] = datetime.now()
        manual_req["ingesting_status"] = "Failed"
        manual_req["general_status"] = "Failed Ingesting"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingesting",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_delete_by_id(request):
    """
    This is a function for updating by id.
    """
    manual_req = {
        "general_status": "Deleting",
        "harvesting_status": request.data.get("harvesting_status")
        if request.data.get("harvesting_status") != "null"
        else None,
        "harvesting_datetime": request.data.get("harvesting_datetime")
        if request.data.get("harvesting_datetime") != "null"
        else None,
        "ingesting_status": request.data.get("ingesting_status")
        if request.data.get("ingesting_status") != "null"
        else None,
        "ingesting_datetime": request.data.get("ingesting_datetime")
        if request.data.get("ingesting_datetime") != "null"
        else None,
    }
    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    APIHarvesterIngesterScheduler.objects.update_or_create(
        id=get_schedulerjob_object.id, defaults=manual_req
    )
    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_schedulerjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_schedulerjob_object.id

    # Making a stac_dir
    stac_dir_get = (
        get_schedulerjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    try:
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_schedulerjob_object.id,
                "logger_name": get_schedulerjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

    except Exception as stac_error:
        manual_req["general_status"] = "Failed Deleting STAC"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {"message": "Failed in deleting TestJobObject." + str(stac_error)},
            status=500,
            safe=False,
        )
    try:
        response = requests.delete(
            f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_delete_object_by_id/",
            data={"name": logger_name_get + "_" + str(logger_id_get)},
        )
        response.raise_for_status()
    except requests.RequestException as logger_api_error:
        manual_req["general_status"] = "Failed Deleting Logger"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": f"Failed in deleting Logger via API: {str(logger_api_error)}"
            },
            status=500,
        )
    try:
        get_schedulerjob_object.delete()

    except ObjectDoesNotExist:
        return JsonResponse({"message": "Object not found"}, status=404)
    except Exception as delete_error:
        JsonResponse(
            {
                "message": "failed in deleting the object from Test Job Table. "
                + str(delete_error)
            },
            status=500,
        )

    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except Exception as directory_error:
            return JsonResponse(
                {
                    "message": f"Failed in deleting local directory: {str(directory_error)}"
                },
                status=500,
            )
    else:
        return JsonResponse(
            {"message": "success"},
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_delete_by_id_force(request):
    """
    This is a function for updating by id.
    """
    manual_req = {
        "general_status": "Deleting",
        "harvesting_status": request.data.get("harvesting_status")
        if request.data.get("harvesting_status") != "null"
        else None,
        "harvesting_datetime": request.data.get("harvesting_datetime")
        if request.data.get("harvesting_datetime") != "null"
        else None,
        "ingesting_status": request.data.get("ingesting_status")
        if request.data.get("ingesting_status") != "null"
        else None,
        "ingesting_datetime": request.data.get("ingesting_datetime")
        if request.data.get("ingesting_datetime") != "null"
        else None,
    }
    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    APIHarvesterIngesterScheduler.objects.update_or_create(
        id=get_schedulerjob_object.id, defaults=manual_req
    )
    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_schedulerjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_schedulerjob_object.id

    # Making a stac_dir
    stac_dir_get = (
        get_schedulerjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )
    base_dir = settings.STAC_FILES
    job_dir = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-"),
    )
    job_dir1 = os.path.join(
        base_dir,
        logger_name_get
        + "-"
        + str(logger_id_get)
        + "-"
        + request.data["email"].replace("@", "-").replace(".", "-")
        + "_1",
    )
    try:
        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Delete",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            # logger_properties={
            #     "logger_handler": "StreamHandler",
            #     "logger_id": logger_id_get,
            #     "logger_name": logger_name_get,
            # }
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_schedulerjob_object.id,
                "logger_name": get_schedulerjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )

    except Exception:
        manual_req["general_status"] = "Force Deleting STAC"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

    try:
        response = requests.delete(
            f"http://{settings.EXTERNAL_URL_CAT4KIT}:8003/api/logger/logger_delete_object_by_id/",
            data={"name": logger_name_get + "_" + str(logger_id_get)},
        )
        response.raise_for_status()
    except requests.RequestException:
        manual_req["general_status"] = "Force Deleting Logger"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

    try:
        get_schedulerjob_object.delete()

    except ObjectDoesNotExist:
        manual_req["general_status"] = "Force Deleting Object"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse({"message": "Object not found"}, status=404)
    except Exception:
        manual_req["general_status"] = "Force Deleting Object"
        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

    if manual_req["harvesting_status"] == "Finished":
        try:
            if os.path.exists(stac_dir_get):
                shutil.rmtree(stac_dir_get)
            if os.path.exists(job_dir):
                shutil.rmtree(job_dir)
            if os.path.exists(job_dir1):
                shutil.rmtree(job_dir1)
            return JsonResponse(
                {"message": "success"},
                safe=False,
            )
        except Exception as directory_error:
            manual_req["general_status"] = "Force Deleting Local Directory"
            APIHarvesterIngesterScheduler.objects.update_or_create(
                id=get_schedulerjob_object.id, defaults=manual_req
            )
            return JsonResponse(
                {
                    "message": f"Failed in deleting local directory: {str(directory_error)}"
                },
                status=500,
            )
    else:
        return JsonResponse(
            {"message": "success"},
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_save_update_close_by_id(request):
    """
    This is a function for updating by id.
    """

    request.data._mutable = True
    # Saniting the job_name
    if request.data["job_name"] == "" or request.data["job_name"].isspace():
        request.data["job_name"] = "TDS2STAC-JOB"
    else:
        request.data["job_name"] = (
            request.data["job_name"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_id
    if request.data["stac_id"] == "" or request.data["stac_id"].isspace():
        request.data["stac_id"] = "cat4kit-id"
    else:
        request.data["stac_id"] = (
            request.data["stac_id"]
            .strip()
            .replace(" ", "-")
            .replace("\n", "-")
            .replace("_", "-")
        )

    # Saniting the stac_title
    if (
        request.data["stac_title"] == ""
        or request.data["stac_title"].isspace()
        or request.data["stac_title"].strip() == ""
    ):
        request.data["stac_title"] = "Cat4KIT Title"

    # Saniting the stac_desc
    if (
        request.data["stac_desc"] == ""
        or request.data["stac_desc"].isspace()
        or request.data["stac_desc"].strip() == ""
    ):
        request.data["stac_desc"] = "cat4kit description"

    # Jsonifying the fields that are not jsonified
    request.data["extra_metadata"] = json.loads(request.data["extra_metadata"])
    try:
        request.data["extra_metadata"]["item"].update(
            {
                "start_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "end_datetime": "It will fill out automatically if set `extra_metadata` to True`",
                "created": "It will fill out automatically if set `extra_metadata` to True`",
                "updated": "It will fill out automatically if set `extra_metadata` to True`",
            }
        )
    except Exception:
        print("There is no item in extra_metadata")
    request.data["asset_properties"] = json.loads(
        request.data["asset_properties"]
    )
    request.data["extension_properties"] = json.loads(
        request.data["extension_properties"]
    )
    request.data["requests_properties"] = json.loads(
        request.data["requests_properties"]
    )
    request.data["webservice_properties"] = request.data[
        "webservice_properties"
    ]
    request.data["stac_dir"] = settings.STAC_DIR

    instance_id = request.data.get("id")
    if instance_id:
        try:
            instance = APIHarvesterIngesterScheduler.objects.get(
                id=instance_id
            )
        except APIHarvesterIngesterScheduler.DoesNotExist:
            print("instance does not exist")
    else:
        instance = None

    form = SaveUpdateCloseForm(request.data, request.FILES, instance=instance)

    if form.is_valid():
        form.save()

    else:
        return JsonResponse(
            form.errors.as_json(),
            safe=False,
        )

    request.data._mutable = True
    get_schedulerjob_object = APIHarvesterIngesterScheduler.objects.filter(
        id=request.data["id"]
    ).first()

    # Making a infra reader logger name and id for  the logger
    logger_name_get = (
        get_schedulerjob_object.job_name.strip()
        .replace(" ", "-")
        .replace("\n", "-")
        .replace("_", "-")
    )
    logger_id_get = get_schedulerjob_object.id

    manual_req = {
        "updating_datetime": None,
        "updating_status": "pending",
    }

    # Making a stac_dir
    stac_dir_get = (
        get_schedulerjob_object.stac_dir
        + "/"
        + logger_name_get
        + "_"
        + str(logger_id_get)
    )

    try:
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Started"
        manual_req["general_status"] = "Updating"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

        InsUpDel4STAC(
            stac_dir=stac_dir_get + "/stac/",
            action="Update",
            service_type="pgstac",
            pgstac_properties={
                "POSTGRES_HOST_READER": "pgstacprod",
                "POSTGRES_HOST_WRITER": "pgstacprod",
                "POSTGRES_PORT": "5432",
                "POSTGRES_USER": "username",
                "POSTGRES_PASSWORD": "password",
                "POSTGRES_HOST": "pgstacprod",
                "PGUSER": "username",
                "PGPASSWORD": "password",
                "PGHOST": "pgstacprod",
                "PGDATABASE": "pgstacprod",
            },
            logger_properties={
                "logger_handler": "HTTPHandler",
                "logger_id": get_schedulerjob_object.id,
                "logger_name": get_schedulerjob_object.job_name,
                "logger_handler_host": settings.EXTERNAL_URL_CAT4KIT,
                "logger_handler_port": "8003",
                "logger_handler_url": "/api/logger/logger_post/",
                "logger_handler_method": "POST",
            },
        )
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Finished"
        manual_req["general_status"] = "Up-to-Date"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )

        return JsonResponse(
            {
                "message": "success",
            },
            safe=False,
        )

    except Exception:
        manual_req["updating_datetime"] = datetime.now()
        manual_req["updating_status"] = "Failed"
        manual_req["general_status"] = "Failed Updating"

        APIHarvesterIngesterScheduler.objects.update_or_create(
            id=get_schedulerjob_object.id, defaults=manual_req
        )
        return JsonResponse(
            {
                "message": "failed in ingestion",
            },
            safe=False,
        )


# TODO: I should narrow down sanitizing the model to the limited number of fields
@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_schedulerjob_objects(request):
    get_schedulerjob_objects = (
        APIHarvesterIngesterScheduler.objects.all().order_by("-created_at")
    )
    serializer = APIHarvesterIngesterSelectedSerializer(
        get_schedulerjob_objects, many=True
    )
    return JsonResponse(serializer.data, safe=False)


# TODO: I should narrow down sanitizing the model to the limited number of fields
@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_schedulerjob_objects_pagination(request):
    search_query = request.query_params.get("search", None)
    get_schedulerjob_objects = APIHarvesterIngesterScheduler.objects.all()

    if search_query is not None:
        queryset = get_schedulerjob_objects.filter(
            Q(job_name__icontains=search_query)
            | Q(email__icontains=search_query)
            | Q(id__icontains=search_query)
            # Add other fields you want to search by
        )
        queryset = queryset.order_by("-created_at")
    else:
        queryset = get_schedulerjob_objects.order_by("-created_at")
    # Your existing logic to filter and order objects

    paginator = PageNumberPagination()
    page_size = request.query_params.get("limit", 30)
    paginator.page_size = int(page_size)
    page = paginator.paginate_queryset(queryset, request)

    if page is not None:
        serializer = APIHarvesterIngesterSelectedSerializer(page, many=True)
        return paginator.get_paginated_response(serializer.data)

    serializer = APIHarvesterIngesterSelectedSerializer(
        get_schedulerjob_objects, many=True
    )
    return JsonResponse(serializer.data)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def get_schedulerjob_objects_by_id(request, id):
    get_schedulerjob_objects = APIHarvesterIngesterScheduler.objects.filter(
        id=id
    ).order_by("-created_at")
    serializer = APIHarvesterIngesterSerializer(
        get_schedulerjob_objects, many=True
    )
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def tds2stac_get_depth_client_by_id(request, id):
    get_schedulerjob_objects = APIHarvesterIngesterScheduler.objects.filter(
        id=id
    ).order_by("-created_at")

    serializer = APIHarvesterIngesterDepthClientSerializer(
        get_schedulerjob_objects, many=True
    )
    return JsonResponse(serializer.data, safe=False)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def get_all_available_webservices(request):
    """Get all available webservices from the catalog."""
    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    try:
        webservices = WebServiceListScraper(
            url=catalog_url,
        )

        return JsonResponse(
            {
                "message": "success",
                "webservices": list(webservices),
            },
        )
    except Exception:
        print(traceback.format_exc())
        return JsonResponse(
            {
                "message": "failed in getting available webservices",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_validate_catalog_url(request):
    """Get all available webservices from the catalog."""
    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    if catalog_url.isspace() or catalog_url == "":
        return JsonResponse(
            {"TDS_status": "TDS Checker msg: No catalog URL provided."}
        )
    else:
        try:
            xml_url = requests.get(
                catalog_url,
                # None,
                # auth=requests_properties["auth"],
                verify=False,
                # timeout=requests_properties["timeout"],
            )
            if xml_url.status_code == 200:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is accessible.",
                    },
                )
            else:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                    },
                    safe=False,
                )
        except BaseException:
            return JsonResponse(
                {
                    "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                },
                safe=False,
            )
