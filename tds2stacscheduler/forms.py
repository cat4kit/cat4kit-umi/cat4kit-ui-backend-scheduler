# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.forms import ModelForm

from .models import APIHarvesterIngesterScheduler


class InitialRecognizingForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = ["id"]
        fields = "__all__"


class HarvesterRecognizerForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = ["id", "created_at"]  # Exclude the 'id' field from the form
        fields = "__all__"  # You can also specify specific fields if needed


class NestedCollectionForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = [
            "id",
            "harvesting_datetime",
            "harvesting_status",
            "ingesting_datetime",
            "ingesting_status",
            "created_at",
        ]  # Exclude the 'id' field from the form
        fields = "__all__"  # You can also specify specific fields if needed


class SaveCloseForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = [
            "id",
            "harvesting_datetime",
            "harvesting_status",
            "ingesting_datetime",
            "ingesting_status",
            "created_at",
        ]  # Exclude the 'id' field from the form
        fields = "__all__"  # You can also specify specific fields if needed


class SaveHarvestIngestCloseForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = [
            "id",
            "created_at",
            "harvesting_datetime",
            "harvesting_status",
            "ingesting_datetime",
            "ingesting_status",
        ]  # Exclude the 'id' field from the form
        fields = "__all__"  # You can also specify specific fields if needed


class SaveUpdateCloseForm(ModelForm):
    class Meta:
        model = APIHarvesterIngesterScheduler
        exclude = ["id", "created_at"]  # Exclude the 'id' field from the form
        fields = "__all__"  # You can also specify specific fields if needed
